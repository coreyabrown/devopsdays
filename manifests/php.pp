# This class will install and setup PHP, PHP FPM and PHP Extensions.
#
# @summary Installs and configures PHP and extensions
#
# @api private
#
# @see devopsdays
# @see https://forge.puppet.com/puppet/php PHP Module
# @see https://secure.php.net/docs.php PHP Docs
# @see https://secure.php.net/manual/en/install.fpm.php PHP FPM Docs
#
# @author David Hollinger <david.hollinger@moduletux.com>
#
class devopsdays::php {
  assert_private()

  class { 'php':
    ensure       => 'present',
    manage_repos => true,
    fpm          => true,
    dev          => true,
    composer     => true,
    pear         => true,
    phpunit      => false,
    settings     => {
      'fpm/cgi.fix_pathinfo' =>  $devopsdays::fix_cgi,
    },
    extensions   => $devopsdays::php_extensions,
  }

  php::fpm::pool { 'example.com':
    listen       => $devopsdays::fpm_skt_dir,
    listen_owner => $devopsdays::fpm_listen_owner,
    listen_group => $devopsdays::fpm_listen_group,
    user         => $devopsdays::fpm_user,
    group        => $devopsdays::fpm_group,
  }
}
